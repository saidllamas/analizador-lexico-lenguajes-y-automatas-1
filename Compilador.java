/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladorparte1;

/**
 *
 * @author said
 */

import java.util.*;

public class Compilador {
    
    private String[] p_reservadas= {"crea", "imprime", "clase", "privada", "publica", "protegida", "verdadero", "falso", "void"};
    private String[] ciclos = {"ciclo_mientras", "ciclo_para"};
    private String[] condicionales = {"compara_si", "sino"};
    private String[] operadores = {"="};
    private String[] agrupadores = {"{", "}", "(", ")","[", "]"};
    private String[] o_aritmeticos = {"+", "-", "*", "/", "^", "++", "--", "*=", "/=", "+=", "-="};
    private String[] relacionales = {"<", ">", "<=", ">=", "==", "!="};
    private String[] numeros = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    private String[] t_datos = {"entero", "flotante", "boleano", "cadena"};
    private String[] fdc = {";"};
    
    public String analizar(String content){
        
        String pre_model = "";
        String renglon = "";
        int n_renglon = 1;
        for(int i = 0; i < content.length(); i++){
            if(content.charAt(i) != '\n')
                renglon += content.charAt(i);
            else { 
                //System.out.println("Renglon: "+n_renglon++);
                pre_model += procesarRenglon(renglon, n_renglon++);
                renglon = "";
            }
        }
        return pre_model;
    }
    
    public String procesarRenglon(String renglon, int n_renglon){
        String palabras = "";
        for(int i = 0; i < renglon.length(); i++){ // lectura todo el renglon
            boolean cadena = false;
            String palabra = "";
            if(renglon.charAt(i) == '"'){
                cadena = true;
                i++;
                while(renglon.charAt(i) != '"'){//lectura toda una cadena
                    palabra += renglon.charAt(i);
                    if(i+1 < renglon.length()){
                        i++;
                    } else break;
                }
            }
            if(!cadena){
                while(renglon.charAt(i) != ' '){//lectura toda una palabra - todas las letras forman una palabra hasta encontrar un espacio, ignora el espacio, sin espacio al inicio ofinal
                    palabra += renglon.charAt(i);
                    if(i+1 < renglon.length()){
                        i++;
                    } else break;
                }
                if(palabra.length() > 0)
                    palabras += n_renglon + "º" +analizarPalabra(palabra) + "º" +palabra +"ª";
            }else{
                //System.out.println("Cadena: "+palabra);
                palabras += n_renglon + "ºCadenaº" +palabra +"ª";
            }
            //palabras.addElement(palabra);
        }
            // aqui puedo continuar analizando la semantica de la oracion-renglon
        return palabras;
    }
    
    public String analizarPalabra(String palabra){
        String familia = "ERROR";
        boolean coincidencia = false;
        for(String p : p_reservadas){
            if(p.equalsIgnoreCase(palabra)){
                //System.out.println(p+" como palabra reservada");
                familia = "RESERVADA";
                coincidencia = true;
            }
        }
        for(String p : ciclos){
            if(p.equalsIgnoreCase(palabra)){
                //System.out.println(p+" como ciclo");
                familia = "CICLO";
                coincidencia = true;
            }
        }
        for(String p : condicionales){
            if(p.equalsIgnoreCase(palabra)){
                //System.out.println(p+" como condicion");
                familia = "CONDICIONAL";
                coincidencia = true;
            }
        }
        for(String p : relacionales){
            if(p.equalsIgnoreCase(palabra)){
                //System.out.println(p+" como operador relacional");
                familia = "RELACIONAL";
                coincidencia = true;
            }
        }
        for(String p : agrupadores){
            if(p.equalsIgnoreCase(palabra)){
                //System.out.println(p+" como operador");
                familia = "AGRUPADOR";
                coincidencia = true;
            }
        }
        for(String p : operadores){
            if(p.equalsIgnoreCase(palabra)){
                //System.out.println(p+" como operador");
                familia = "OPERADOR";
                coincidencia = true;
            }
        }
        for(String p : o_aritmeticos){
            if(p.equalsIgnoreCase(palabra)){
                //System.out.println(p+" como operador aritmeticos");
                familia = "ARITMETICO";
                coincidencia = true;
            }
        }
        
        for(String p : t_datos){
            if(p.equalsIgnoreCase(palabra)){
                //System.out.println(p+" como tipo de dato");
                familia = "TIPO DATO";
                coincidencia = true;
            }
        }
        for(String p : fdc){
            if(p.equalsIgnoreCase(palabra)){
                familia = "FDC";
                coincidencia = true;
            }
        }
        
        
        
        if(!coincidencia){
            int iteraciones = palabra.length();
            String tmp_numero = "";
            boolean terminar = false;
            for(int i = 0; i < iteraciones; i++){
                for(String p : numeros){
                    if(p.equalsIgnoreCase(palabra.charAt(i)+"")){
                        tmp_numero += palabra.charAt(i);
                        coincidencia = true;
                    }
                    if((palabra.charAt(i) > 57 || palabra.charAt(i) < 48) && palabra.charAt(i) != 46){ terminar = true; coincidencia = false; break;}
                }
                if(terminar) break;
            }
            if(coincidencia){
                //System.out.println(tmp_numero+" como numero");
                familia = "ENTERO";
                for(int i = 0; i < palabra.length(); i++) if(palabra.charAt(i) == 46) familia = "REAL";
            }
            if(!coincidencia){
                if(!analizarCaracteres(palabra))
                    familia = validarVariable(palabra);
            }
            
        }   
        return familia;
    }
    
    private String validarVariable(String palabra){
        String familia = "";
        if(palabra.length()>0){
            familia = "VARIABLE";
            if( (palabra.charAt(0) >= 65 && palabra.charAt(0) <= 90) || (palabra.charAt(0) >= 97 && palabra.charAt(0) <= 122) ){ //inicia con a-z o A-Z
                for(int i = 0;  i < palabra.length(); i++) {
                    if(palabra.charAt(i) < 48 || (palabra.charAt(i) > 57 && palabra.charAt(i) < 65) || (palabra.charAt(i) > 90 && palabra.charAt(i) < 97) || palabra.charAt(i) > 122  ){
                        familia = "ERROR";
                        break;
                    }
                }
            } else familia = "ERROR";
        }
        return familia;
    }
    
    public boolean analizarCaracteres(String palabra){
        boolean identificado = false;
        for(int i = 0; i < palabra.length(); i++){
            String pal = ""+ palabra.charAt(i);
            for(String p : operadores){
                if(p.equals(pal)){
                    System.out.println(p+" como operador");
                    identificado = true;
                }
            }
            if( (i+1) < palabra.length()){
                if(palabra.charAt(i) == '+' || palabra.charAt(i) == '-' || palabra.charAt(i) == '*' || palabra.charAt(i) == '/'){ // vs++ vs--
                    if(palabra.charAt(i) == palabra.charAt(i+1)){
                        String tmp = palabra.charAt(i) + "" + palabra.charAt(i+1);
                        for(String p : o_aritmeticos){
                            if(p.equals(tmp)){
                                System.out.println(p+" como operador aritmeticos");
                                identificado = true;
                            }
                        }
                    } else System.out.println("Error, no coinciden ambos operadores aritmeticos");

                }
            }
                
        }
            
        return identificado;
    }
}
