/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladorparte1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author said
 */
public class Depurador {
    
    public String muestraContenido(String archivo) throws FileNotFoundException, IOException {
        String cadena, new_file_content = "";
        FileReader f = new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        boolean brincar = true;
        
        while( ( cadena = b.readLine() ) != null ) {
            if(cadena.length() > 0){
                for(int i = 0; i < cadena.length(); i++){
                    brincar = true;
                    if( i+1 == cadena.length() ){
                        if(cadena.charAt(i)!=' ' || cadena.charAt(i)!='\n' )
                            new_file_content += cadena.charAt(i);
                        break;
                    }
                    if(cadena.charAt(i) == '/' && cadena.charAt(i+1) == '/' ){
                        //new_file_content += ""; 
                        brincar = false;
                        break;
                    }else if(cadena.charAt(i) == ' '){
                        new_file_content += " ";
                        while(cadena.charAt(i) == ' '){
                            if(cadena.length() > i)
                                i++;
                            if(i == cadena.length()){
                                brincar = true;
                                break;
                            }
                        }
                        i--;
                        brincar = false;
                    } else
                        new_file_content += cadena.charAt(i);
                }
            }
            if(brincar && cadena.length()>0) new_file_content += "\n";
        }
        
        b.close();
        //return new_file_content;
        return segundoFiltro(new_file_content);
    }
    
    public String segundoFiltro(String content){
        String new_content = "";
        for(int i = 0; i < content.length(); i++){
            if(content.charAt(i) == ' '){
                if(content.charAt(i-1) != '\n')
                    new_content += content.charAt(i);
            } else new_content += content.charAt(i);
        }
        return tercerFiltro(new_content);
    }
    
    public String tercerFiltro(String content){
        String new_content = "";
        for(int i = 0; i < content.length(); i++){
            if(content.charAt(i) == ' '){
                if(content.charAt(i-1) != '\n')
                    new_content += content.charAt(i);
            } else new_content += content.charAt(i);
        }
        return new_content;
    }
    
    public String depura() throws FileNotFoundException, IOException {
        String cadena, new_file_content = "";
        FileReader f = new FileReader("/users/said/desktop/temp.txt");
        BufferedReader b = new BufferedReader(f);
        boolean brincar = true;
        
        while( ( cadena = b.readLine() ) != null ) {
            if(cadena.length() > 0){
                for(int i = 0; i < cadena.length(); i++){
                    brincar = true;
                    if( i+1 == cadena.length() ){
                        if(cadena.charAt(i)!=' ' || cadena.charAt(i)!='\n' )
                            new_file_content += cadena.charAt(i);
                        break;
                    }
                    if(cadena.charAt(i) == '/' && cadena.charAt(i+1) == '/' ){
                        //new_file_content += ""; 
                        brincar = false;
                        break;
                    }else if(cadena.charAt(i) == ' '){
                        new_file_content += " ";
                        while(cadena.charAt(i) == ' '){
                            if(cadena.length() > i)
                                i++;
                            if(i == cadena.length()){
                                brincar = true;
                                break;
                            }
                        }
                        i--;
                        brincar = false;
                    } else
                        new_file_content += cadena.charAt(i);
                }
            }
            if(brincar && cadena.length()>0) new_file_content += "\n";
        }
        
        b.close();
        //return new_file_content;
        return segundoFiltro(new_file_content);
    }
}
