/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladorparte1;

import java.io.File;
import java.io.FileWriter;

/**
 *
 * @author said
 */
public class Escritura {
    public void escribirArchivo(String nombreArchivo, String contenido){
        try {
            //Crear un objeto File se encarga de crear o abrir acceso a un archivo que se especifica en su constructor
            File archivo = new File( nombreArchivo );

            //Crear objeto FileWriter que sera el que nos ayude a escribir sobre archivo
            FileWriter escribir = new FileWriter(archivo,true);

            //Escribimos en el archivo con el metodo write 
            escribir.write(contenido);

            //Cerramos la conexion
            escribir.close();
        } catch(Exception e) {
            System.out.println("Error al escribir");
            System.out.println(e.getCause()+" "+e.getMessage());
            
        }
    }
    
    public void escribe(String nombreArchivo, String contenido){
        try {
            //Crear un objeto File se encarga de crear o abrir acceso a un archivo que se especifica en su constructor
            File archivo = new File( nombreArchivo );
            
            if(archivo.exists()){ System.out.println("El archivo existe. eliminando..."); archivo.delete();}
            
            //Crear objeto FileWriter que sera el que nos ayude a escribir sobre archivo
            FileWriter escribir = new FileWriter(archivo,true);

            //Escribimos en el archivo con el metodo write 
            escribir.write(contenido);

            //Cerramos la conexion
            escribir.close();
        } catch(Exception e) {
            System.out.println("Error al escribir");
            System.out.println(e.getCause()+" "+e.getMessage());
            
        }
    }
}
