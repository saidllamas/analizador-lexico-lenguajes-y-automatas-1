/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladorparte1;

/**
 *
 * @author said
 */
public class Principal {
    private static String rutaABS = "/users/said/desktop/";
    private static String extIN = ".txt", extOut = ".sllm";
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Depurador depurador = new Depurador();
        Escritura escritura = new Escritura();
        Compilador compilador = new Compilador();
        
        try{
            String nombre = "fuente";
            String content = depurador.muestraContenido(rutaABS+ nombre +extIN);
            //content = compilador.analizar(content);
            compilador.analizar(content);
            escritura.escribirArchivo(rutaABS+ nombre +extOut, content);
            //System.out.println(content);
        } catch(Exception e){
            System.out.println("Error al leer");
            System.out.println(e.getCause()+" "+e.getMessage());
        }
    }
    
}
