# Analizador Léxico - Lenguajes y Autómatas 1

Código fuente del proyecto realizado en la materia Lenguajes y Autómatas 1. El programa analiza un código fuente, lo depura eliminando los espacios dobles, renglones vacíos y tabuladores para posteriormente hacer la búsqueda de los lexemas.